﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AmazonViewerWebApp.CurrencyWsdl;
using AmazonViewerWebApp.Models;
using PagedList;

namespace AmazonViewerWebApp.ViewModels
{
    public class IndexViewModel
    {      
        public IPagedList<AmazonItem> AmazonItems;    
    }
}