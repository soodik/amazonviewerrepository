﻿using AmazonViewerWebApp.Services.Amazon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AmazonViewerWebApp.Services.Currency;

namespace AmazonViewerWebApp.Controllers.Api
{
    public class AmazonItemController : ApiController
    {
        private IAmazonApiConnector _amazonApiConnector;
        private ICurrencyService _icurrencyService;

        public AmazonItemController(IAmazonApiConnector amazonApiConnector, ICurrencyService currencyService)
        {
            _amazonApiConnector = amazonApiConnector;
            _icurrencyService = currencyService;
        }
    }
}
