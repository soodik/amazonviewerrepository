﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Mvc;
using AmazonViewerWebApp.AmazonService;
using AmazonViewerWebApp.CurrencyWsdl;
using AmazonViewerWebApp.Models;
using AmazonViewerWebApp.Services.Amazon;
using AmazonViewerWebApp.Services.Currency;
using AmazonViewerWebApp.ViewModels;
using PagedList;

namespace AmazonViewerWebApp.Controllers.Web
{
    public class HomeController : Controller
    {
        private IAmazonApiConnector _amazonApiConnector;
        static int _pageNr = 1;
        private ICurrencyService _currencyService;

        public HomeController(IAmazonApiConnector amazonApiConnector, ICurrencyService currencyService)
        {

            _amazonApiConnector = amazonApiConnector;
            _currencyService = currencyService;
        }

        public ActionResult Index(string searchString, string currencyText, int? page)
        {
            Session["searchString"] = searchString;
    
            List<AmazonItem> itemList = new List<AmazonItem>();
            IndexViewModel model = new IndexViewModel();

            if (string.IsNullOrEmpty(currencyText))
            {

                Session["currencyText"] = Currency.USD;
            }
            else
            {
                Session["currencyText"] = currencyText;
            }

           
            if (searchString != null)
            {
                var result = _amazonApiConnector.GetAmazonData(searchString);
                itemList = result;
            }

            UpdateAmazonItemList(currencyText, itemList);

            int pageSize = 13;
            _pageNr = (page ?? 1);
            int pageNumber = (page ?? 1);

            model.AmazonItems = itemList.ToPagedList(pageNumber, pageSize);

            return View("Index", model);
        }
        [HttpGet]
        public PartialViewResult ChangeCurrency(string searchString, string currencyText, int? page)
        {
            IndexViewModel model = new IndexViewModel();
            Session["currencyText"] = currencyText;
            Session["searchString"] = searchString;
            List<AmazonItem> itemList = new List<AmazonItem>();
            if (searchString != null)
            {
                var result = _amazonApiConnector.GetAmazonData(searchString);
                itemList = result;
            }
            UpdateAmazonItemList(currencyText, itemList);
            
            int pageSize = 13;
            _pageNr = (page ?? 1);
            int pageNumber = (page ?? 1);

            model.AmazonItems = itemList.ToPagedList(pageNumber, pageSize);

            return PartialView("AmazonPartialView", model);
        }

        private List<AmazonItem> UpdateAmazonItemList(string currencyText, List<AmazonItem> itemList)
        {
            if (!string.IsNullOrEmpty(currencyText))
            {
                var currencyList = Enum.GetValues(typeof(CurrencyWsdl.Currency))
                    .Cast<CurrencyWsdl.Currency>().ToList();

                Currency selectedCurrency = currencyList.FirstOrDefault(x => x.ToString() == currencyText);

                var rate = _currencyService.GetCurrencyRate(Currency.USD, selectedCurrency);
                foreach (var items in itemList)
                {
                    items.Price = items.Price * Convert.ToDecimal(rate);
                }
            }

            return itemList;
        }
    }
}