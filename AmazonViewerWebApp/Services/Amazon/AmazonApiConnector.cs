﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using AmazonViewerWebApp.AmazonService;
using AmazonViewerWebApp.Models;

namespace AmazonViewerWebApp.Services.Amazon
{
    public interface IAmazonApiConnector
    {
        List<AmazonItem> GetAmazonData(string mySearch);
    }
    class AmazonApiConnector : IAmazonApiConnector
    {
        //Sisesta vajalikud andmed
        private string _accessKeyId = "sisesta";
        private string _secretKey = "sisesta";
        private string _associateTag = "sisest";
        private AWSECommerceServicePortTypeClient _client;
        private int _pageAmount = 2;

        public AmazonApiConnector()
        {
            BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.Transport);
            binding.MaxReceivedMessageSize = int.MaxValue;
            _client = new AWSECommerceServicePortTypeClient(
                binding,
                new EndpointAddress("https://webservices.amazon.com/onca/soap?Service=AWSECommerceService"));

            // add authentication to the ECS client
            _client.ChannelFactory.Endpoint.Behaviors.Add(new AmazonSigningEndpointBehavior(_accessKeyId, _secretKey));
        }

        public List<AmazonItem> GetAmazonData(string mySearch)
        {
            ItemSearch itemSearch = new ItemSearch();
            var searchList = new List<ItemSearchRequest>();
            //Make Batch requests !
            for (int i = 0; i < _pageAmount; i++)
            {
                ItemSearchRequest request = new ItemSearchRequest
                {
                    SearchIndex = "Books",
                    Title = mySearch,
                    ItemPage = (i + 1).ToString(),
                    ResponseGroup = new string[] { "Medium" }
                };
                searchList.Add(request);
            }

            itemSearch.Request = searchList.ToArray();
            itemSearch.AWSAccessKeyId = _accessKeyId;
            itemSearch.AssociateTag = _associateTag;

            // issue the ItemSearch request
            ItemSearchResponse response = _client.ItemSearch(itemSearch);
            List<AmazonItem> itemList = new List<AmazonItem>();
            foreach (var responseItems in response.Items)
            {
                foreach (var item in responseItems.Item)
                {
                    AmazonItem amazonItem = new AmazonItem
                    {
                        Title = item.ItemAttributes.Title,
                        Price = Convert.ToDecimal(item.ItemAttributes.ListPrice.Amount),
                        
                    };
                    itemList.Add(amazonItem);
                }
            }

            return itemList;

        }
    }
}