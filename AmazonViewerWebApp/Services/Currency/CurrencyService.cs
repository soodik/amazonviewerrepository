﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Xml.Linq;
using AmazonViewerWebApp.CurrencyWsdl;

namespace AmazonViewerWebApp.Services.Currency
{
    public interface ICurrencyService
    {
        double GetCurrencyRate(CurrencyWsdl.Currency oldCurrency, AmazonViewerWebApp.CurrencyWsdl.Currency newCurrency);
    }


    public class CurrencyService : ICurrencyService
    {
        readonly CurrencyConvertorSoapClient _client;
        public CurrencyService()
        {
            _client = new CurrencyConvertorSoapClient("CurrencyConvertorSoap12");
        }

        public double GetCurrencyRate(CurrencyWsdl.Currency oldCurrency, AmazonViewerWebApp.CurrencyWsdl.Currency newCurrency)
        {
            var returnValue = _client.ConversionRate(oldCurrency, newCurrency);
            return returnValue;
        }
    }
}