﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AmazonViewerWebApp.Models
{
    public class AmazonItem
    {
        public string Title { get; set; }
        public decimal Price { get; set; }
     
    }
}