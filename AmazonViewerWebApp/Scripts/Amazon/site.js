﻿$(document).ready(function () {
    $("#currencyText").change(currencyChanged);
});

function currencyChanged() {
    var originalText = $("#currencyText").val();
    $.ajax({
        type: "GET",
        url: "/Home/ChangeCurrency",
        data: {
            currencyText: $("#currencyText").val(),
            searchString: $("#searchString").val(),
            page: $(".pagination").children(".active").children("a").text()
        },
        success: function (data) {

            $("#amazonPartialView").html(data);
            $("#currencyText").val(originalText);
        },
        error: function (err) {
            alert(err.error);
        }
    });
}